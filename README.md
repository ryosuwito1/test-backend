# Django Movie App

This Django Movie App is a simple project that manages movie information, including models for Movies, Genres, and Ratings. The project utilizes Python 3.10.11 and uses SQLite as the database.

## Setup and Configuration

### Installation

1. Make sure you have Python 3.10.11 installed.

2. Install the required dependencies:

   ```bash
   pip install -r requirements.txt
   ```

### Running the Project

3. The models have been migrated to SQLite, and Models, Genres, and Ratings are manageable in the admin page.

    A superuser has been created with the username 'admin' and password 'password'.

    The default image has been assigned to all existing movies in the JSON file.

    To run the project, use the command: python manage.py runserver localhost:8000.

    To access the admin page, go to localhost:8000/admin.