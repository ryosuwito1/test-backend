import json
import os
from django.core.management.base import BaseCommand
from movies.models import Movie, Genre, MpaaRating

class Command(BaseCommand):
    help = 'Seed the database with data from movies.json'

    def handle(self, *args, **options):
        base_dir = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
        json_file_path = os.path.join(base_dir, 'movies.json')

        with open(json_file_path, 'r') as file:
            movies_data = json.load(file)

        for movie_data in movies_data:
            genres = [Genre.objects.get_or_create(name=genre)[0] for genre in movie_data['genre']]

            mpaa_rating_data = movie_data['mpaaRating']
            mpaa_rating = MpaaRating.objects.get_or_create(
                type=mpaa_rating_data['type'],
                label=mpaa_rating_data['label']
            )[0]

            movie = Movie.objects.get_or_create(
                id=movie_data['id'],
                name=movie_data['name'],
                description=movie_data['description'],
                imgPath=movie_data['imgPath'],
                duration=movie_data['duration'],
                language=movie_data['language'],
                mpaaRating=mpaa_rating,
                userRating=movie_data['userRating']
            )[0]

            movie.genre.set(genres)

        self.stdout.write(self.style.SUCCESS('Database seeding complete'))
