from django.db.models import Q
from django.http import JsonResponse
from django.shortcuts import render, get_object_or_404
from .models import Movie

def movie_list(request):
    query = request.GET.get('q')

    if query:
        movies = Movie.objects.filter(
            Q(name__icontains=query) | 
            Q(description__icontains=query) | 
            Q(language__icontains=query) | 
            Q(genre__name__icontains=query)
        ).distinct()
    else:
        movies = Movie.objects.all()
    print(request.headers.get('x-requested-with'))
    if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        # If it's an AJAX request, return JSON response
        movie_list_data = [{'id': movie.id, 'name': movie.name, 'imgPath': movie.imgPath.url, 'duration': movie.duration, 'userRating': movie.userRating} for movie in movies]
        return JsonResponse(movie_list_data, safe=False)
    else:
        # If it's a regular request, render the HTML template
        return render(request, 'movies/movie_list.html', {'movies': movies, 'query': query})


def movie_detail(request, movie_id):
    movie = get_object_or_404(Movie, pk=movie_id)
    return render(request, 'movies/movie_detail.html', {'movie': movie})
