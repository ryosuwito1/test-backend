# movies/models.py

from django.db import models

class MpaaRating(models.Model):
    type = models.CharField(max_length=10)
    label = models.CharField(max_length=255)

    def __str__(self):
        return self.type

class Genre(models.Model):
    name = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.name

class Movie(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=255)
    description = models.TextField()
    imgPath = models.ImageField(upload_to='movie_images/')
    duration = models.IntegerField()
    genre = models.ManyToManyField(Genre)
    language = models.CharField(max_length=50)
    
    # MPAA Rating
    mpaaRating = models.ForeignKey(MpaaRating, on_delete=models.CASCADE, default=None)
    
    userRating = models.CharField(max_length=5)

    def __str__(self):
        return self.name
